# README #

### What is this repository for? ###

* Deployment package of a service that enables geocoding function for 4 objects: Location, Job, Resource, Activity, using Google APIs.
* Version 0.1

### How do I get set up? ###

* Download the deployment package
* Update credentials of target organization in build.properties
* Deploy (or validate) the package using [Force.com Migration Tool](https://developer.salesforce.com/page/Force.com_Migration_Tool)
```
#!bash

ant deployCode -verbose
ant validateCode -verbose 
```

* Create a google account, generate a Google API key and enable Google Map Geocoding API and Distance Matrix API
* Get API Token from Skedulo Admin Console 
* Create a custom setting to store Google API Key (optional) and Skedulo API Token. Skedulo API Token is required if Geocoding_Service = 'Skedulo'. Google API Key is required if Geocoding_Service = 'Google'
* Add a Remote Site for Google Map URL (https://maps.googleapis.com)
* Create a record of skedConfigs__c to store Google API Key. 
```
#!java

skedConfigs__c c1 = new skedConfigs__c(Name='Google_API_KEY', Value__c='AIzaSyDG72vVxBIfBXO91BecFK7HLzCto8HuRBs');
skedConfigs__c c2 = new skedConfigs__c(Name='Skedulo_API_Token', Value__c='AIzaSyDG72vVxBIfBXO91BecFK7HLzCto8HuRBs');
skedConfigs__c c3 = new skedConfigs__c(Name='Geocoding_Service', Value__c='Skedulo');

insert new list<skedConfigs__c>{c1, c2, c3};
```

### Calling the service ###
Location Service can be called from a trigger, a controller, apex batch, etc. Below is an  example of calling the service from a trigger
```
#!java

trigger skedLocationTrigger on sked__Location__c (before insert, before update, before delete, after insert, after update, after delete) {

	if(Trigger.isAfter){
		if(Trigger.isInsert || Trigger.IsUpdate){
			skedLocationServices.updateLocationGeocode(Trigger.old, Trigger.newMap, skedLocationServices.OBJ_LOCATION);
		}
	}
}

trigger skedJobTrigger on sked__Job__c (before insert, before update, before delete, after insert, after update, after delete) {
    if (Trigger.isAfter) {
        if(Trigger.isInsert || Trigger.isUpdate){
    		skedLocationServices.updateLocationGeocode(Trigger.old, Trigger.newMap, skedLocationServices.OBJ_JOB);
    	}
    }
    
}

trigger skedResourceTrigger on sked__Resource__c (before insert, before update, before delete, after insert, after update, after delete) {
	
	if (Trigger.isAfter) {
    	if(Trigger.isInsert || Trigger.isUpdate){
    		skedLocationServices.updateLocationGeocode(Trigger.old, Trigger.newMap, skedLocationServices.OBJ_RESOURCE);
    	}
	}
	
}
```

### Who do I talk to? ###

* Tu Dao (tdao@skedulo.com)