@isTest
public class skedTestDataFactory {
	
	/*
	*
	*/
	public static void createCustomSettings(){
		skedConfigs__c c1 = new skedConfigs__c(Name='Google_API_KEY', Value__c='AIzaSyDG72vVxBIfBXO91BecFK7HLzCto8HuRBs');
        skedConfigs__c c2 = new skedConfigs__c(Name='Skedulo_API_Token', Value__c='AIzaSyDG72vVxBIfBXO91BecFK7HLzCto8HuRBs');
        skedConfigs__c c3 = new skedConfigs__c(Name='Geocoding_Service', Value__c='Skedulo');

        insert new list<skedConfigs__c>{c1, c2, c3};
	}

	/*
    * Initializes a list of Account
    */
    public static Account createAccount(String name){
        return new Account(  
                        Name 			= name,
                        ShippingStreet  = '79 McLachlan St Fortitude Valley'
                    ) ;
    }

    /*
	* Get Record Type ID by Name
	*/
	public static Id getRecordTypeId(Schema.DescribeSObjectResult d, String name){
		Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
		if(rtMapByName.containsKey(name)){
			return rtMapByName.get(name).getRecordTypeId();
		}
		return null;
	}

    /*
    * Initializes a list of Job
    */
    public static sked__Job__c createJob(Id accountId, Id regionId){
        return new sked__Job__c( 
                                sked__Account__c		= accountId,
                                sked__Start__c 			= System.now().addHours(9),
                                sked__Finish__c 		= System.now().addHours(10),
                                sked__Duration__c		= 60,
                                sked__Region__c			= regionId,
                                sked__Address__c 		= 'Test Addr'
                            ) ;
    }

    /*
    * Allocate a job to a resource
    */
    public static sked__Job_Allocation__c allocateJob(Id jobId, Id resourceId){
        return new sked__Job_Allocation__c(
				sked__Job__c 			= jobId,
				sked__Resource__c 		= resourceId,
				sked__Assigned_To__c 	= resourceId
			);
    }

    /*
    * Initializes an Activity
    */
    public static sked__Activity__c createActivity(Id resourceId, String timezone){
        
        return new sked__Activity__c(
				sked__Address__c 	= '28 Tuckett Rd, Salisbury, Queensland, AUS',
				sked__Type__c 		= 'Meal Break',
				sked__Start__c 		= System.now(),
				sked__End__c 		= System.now().addMinutes(120),
				sked__Notes__c 		= 'Have a nice lunch',
				sked__Timezone__c 	= timezone==null?'Australia/Brisbane':timezone,
				sked__Resource__c 	= resourceId
			);
    }

    /*
    * Initializes an Event
    */
    public static Event createEvent(Id userId){
        
        return new Event(
			Subject 		= 'Test',
	  		OwnerId 		= userId,
	  		Location 		= 'Test',
	  		StartDateTime = System.Now(),
	  		EndDateTime 	= System.Now().addHours(1),
	  		Description 	= 'Test'
		);
    }


    /*
    * Initializes a Region
    */
    public static sked__Region__c createRegion(String name, String timezone){
        
        sked__Region__c region = new sked__Region__c(
        	Name = name,
            sked__Timezone__c = timezone==null?'Australia/Melbourne':timezone,
            sked__Country_Code__c = 'AU'
        );
			
		return region;
    }

    /*
    * Initializes a Location
    */
    public static sked__Location__c createLocation(String name, Id accountId, Id regionId){
        
        sked__Location__c location = new sked__Location__c(
        	Name 				= name,
            sked__Account__c 	= accountId,
            sked__Region__c 	= regionId,
            sked__Address__c 	= 'Test Addr'
        );
			
		return location;
    }

	/*
    * Initializes a Contact
    */
    public static Contact createContact(Id accountId, String name, Id regionId){
        return new Contact(
        		FirstName 		= name,
        		LastName 		= name,
        		AccountId 		= accountId, 
        		sked__Region__c = regionId,
        		Birthdate 		= System.today().addDays(-10000),
        		otherStreet 	= 'Test',
        		otherCity 		= 'Test',
        		otherState 		= 'Test',
        		otherPostalCode = 'Test',
        		otherCountry 	= 'Test',
        		Phone 			= '1234567890',
        		Email 			= 'test@test.com'
        	);
    }

    /*
    * Initializes a Tag
    */
    public static sked__Tag__c createTag(String name){
        
        return new sked__Tag__c(
				name 			= name,
				sked__Type__c 	= 'Skill',
				sked__Classification__c = 'Global'
			);
    }

    /*
    * Initializes a Resource
    */
    public static sked__Resource__c createResource(String name, Id userId, Id regionId){
        return new sked__Resource__c(
				name 					= name,
				sked__Resource_Type__c 	= 'Person',
				sked__Primary_Region__c = regionId,
				sked__Category__c 		= 'Customer Service',
				sked__Country_Code__c 	= 'AU',
				sked__Home_Address__c 	= '24 Tuckett Rd, Salisbury, Queensland, AUS',
				sked__Is_Active__c 		= true,
				sked__Weekly_Hours__c 	= 40,
				sked__User__c 			= userId
			);
    }

    /*
    * Initializes an Availability record
    */
    public static sked__Availability__c createAvailability(Id resourceId, boolean isAvailable, String timezone){
        
        return new sked__Availability__c(
				sked__Resource__c 		= resourceId,
				sked__Timezone__c 		= timezone==null?'Australia/Brisbane':timezone,
				sked__Is_Available__c 	= isAvailable,
				sked__Type__c 			= 'Available',
				sked__Status__c 		= 'Approved',
				sked__Start__c 			= System.now(),
				sked__Finish__c 		= System.now().addDays(1)
			);
    }
}