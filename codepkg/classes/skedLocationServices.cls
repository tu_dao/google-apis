global class skedLocationServices{
    
    public static final String OBJ_LOCATION     = 'sked__Location__c';
    public static final String OBJ_JOB          = 'sked__Job__c';
    public static final String OBJ_RESOURCE     = 'sked__Resource__c';
    public static final String OBJ_ACTIVITY     = 'sked__Activity__c';
    public static final String GOO_END_POINT    = 'https://maps.googleapis.com/maps/api';
    public static final String SKED_END_POINT    = 'https://app.skedulo.com/suggest/api/geocode?';

    public static final Decimal M_TO_MILES = 0.000621371;

    public static Integer BATCH_SIZE            = 50;

    public static String GOOGLE_API_KEY{
        get{
            if(GOOGLE_API_KEY == null){
                if(skedConfigs__c.getAll().containsKey('Google_API_KEY')){
                    GOOGLE_API_KEY     = skedConfigs__c.getAll().get('Google_API_KEY').Value__c;
                }else GOOGLE_API_KEY   = '';
            }
            return GOOGLE_API_KEY;
        }
    }

    public static String SKED_API_TOKEN{
        get{
            if(SKED_API_TOKEN == null){
                if(skedConfigs__c.getAll().containsKey('Skedulo_API_Token')){
                    SKED_API_TOKEN     = skedConfigs__c.getAll().get('Skedulo_API_Token').Value__c;
                }else SKED_API_TOKEN   = '';
            }
            return SKED_API_TOKEN;
        }
    }

    public static String GEOCODING_SERVICE{
        get{
            if(GEOCODING_SERVICE == null){
                if(skedConfigs__c.getAll().containsKey('Geocoding_Service')){
                    GEOCODING_SERVICE     = skedConfigs__c.getAll().get('Geocoding_Service').Value__c;
                }else GEOCODING_SERVICE   = 'Skedulo';
            }
            return GEOCODING_SERVICE;
        }
    }

    public static Map<String,String> ADDR_FIELD_MAP = new Map<String,String>{
        OBJ_LOCATION    => 'sked__Address__c',
        OBJ_JOB         => 'sked__Address__c',
        OBJ_RESOURCE    => 'sked__Home_Address__c',
        OBJ_ACTIVITY    => 'sked__Address__c'
    };

    

    /*
    * Populate Location Geocode when address is changed
    */
    public static void updateLocationGeocode(list<sObject> oldList, Map<Id, sObject> newMap, String objectType){
        if(System.isFuture()) return;
        if(!ADDR_FIELD_MAP.containsKey(objectType)) return;

        Set<Id> locationIDs = new Set<Id>();
        String addressField = ADDR_FIELD_MAP.get(objectType);
        if(oldList == null){//Insert
            locationIDs = newMap.keySet();
        }else{//Update
            sObject newLoc;
            for(sObject oldLoc : oldList){
                newLoc = newMap.get(oldLoc.Id);
                if(newLoc.get(addressField) != null && newLoc.get(addressField) != oldLoc.get(addressField)){
                    locationIDs.add(newLoc.Id);
                }
            }
        }
        if(locationIDs==null || locationIDs.isEmpty()) return;
        list<Id> idList = new list<Id>(locationIDs);
        if(idList.size() > BATCH_SIZE){//Process in batch to avoid callout limit
            skedBatchProcessor batch = new skedBatchProcessor(String.join(idList,';'), objectType);
            Database.executeBatch(batch, BATCH_SIZE);
        }else updateGeoLocationAsync(String.join(idList,';'), objectType);
    }

    /*
    * Update geolocation for a list of record
    */
    global static void updateGeoLocation(list<String> recordIds, string objectType) {
        if(!ADDR_FIELD_MAP.containsKey(objectType)) return;
        
        String query = 'Select Id, ' + ADDR_FIELD_MAP.get(objectType)  + '  from ' + objectType + ' where Id IN :recordIds';
       
         List<sObject> records = Database.query(query);
        String addressField = ADDR_FIELD_MAP.get(objectType);
        for (sObject record : records) {
            
            Location geoData = skedLocationServices.getGeolocation( (String)record.get(addressField) );
            system.debug(geoData);
            if (geoData != null) {
                record.put('sked__GeoLocation__latitude__s', geoData.getLatitude());
                record.put('sked__GeoLocation__longitude__s', geoData.getLongitude());  
            }
        }
        update records;
    }

    /*
    * Asynchronously update geolocation for a list of record
    */
    @future(callout=true)
    global static void updateGeoLocationAsync(String idList, string objectType) {
        if(!System.isFuture() || System.isBatch()) return;//cannot call a future method from a future or batch context
        string[] recordIds = idList.split(';');
        updateGeoLocation(recordIds, objectType);
    }

    /*
    * Get Geolocation from job Address
    */
    public static Location getGeolocation(string address){
        if( 'Skedulo'.equalsIgnoreCase(GEOCODING_SERVICE) ) return getGeolocationSkeduloAPI(address);
        return getGeolocationGoogleAPI(address);
    }
    
    /*get GeoLocation using Google API*/
    public static Location getGeolocationGoogleAPI(string address){
        
        address = EncodingUtil.urlEncode(address, 'UTF-8');
        // build callout
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        STRING endPointString = GOO_END_POINT + '/geocode/json?address='+address+'&key=' + GOOGLE_API_KEY;
        
        req.setEndpoint(endPointString);
        req.setMethod('GET');
        req.setTimeout(10000);
        
        try{
            // callout
            HttpResponse res = h.send(req);
            // parse coordinates from response
            JSONParser parser = JSON.createParser(res.getBody());
            double lat = null;
            double lon = null;
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                    (parser.getText() == 'location')){
                        parser.nextToken(); // object start
                        while (parser.nextToken() != JSONToken.END_OBJECT){
                            String txt = parser.getText();
                            parser.nextToken();
                            if (txt == 'lat')
                                lat = parser.getDoubleValue();
                            else if (txt == 'lng')
                                lon = parser.getDoubleValue();
                        }
                        
                    }
            }
            Location obj =  Location.newInstance(lat,lon);
            return obj;
            
        } catch (Exception e) {
        }
        return null;
    }

    /*get GeoLocation using Skedulo API*/
    public static Location getGeolocationSkeduloAPI(string address){
        
        Location geoLoc = new Location();
        if (string.isBlank(address)){
            return null;
        }
        Http http           = new Http();
        HttpRequest req     = new HttpRequest(); 
        HttpResponse res    = new HttpResponse();
        
        if (string.isBlank(SKED_API_TOKEN)) {
            system.debug('#Skedulo API Error: API Token is null#');
            return geoLoc;
        }
        //Set end point to Authenciate
        string params = EncodingUtil.urlEncode(address, 'UTF-8');
        String EndPoint = SKED_END_POINT+ 'addrs=' + params;
        req.setEndpoint( EndPoint );
        req.setMethod('GET');
        req.setTimeout(10000);               
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setHeader('apiToken', SKED_API_TOKEN);
        integer responseCode = 0;
        string jsonResponse = ''; 
        
        res = http.send(req);
        responseCode = res.getStatusCode(); 
        
        if (responseCode == 200) {
            try {
                jsonResponse = res.getBody();
                system.debug(jsonResponse);
                map<string,object> selectedSlot = (map<string,object>)System.JSON.deserializeUntyped(jsonResponse);
                
                Map<String, Object> p = (Map<String, Object>) (selectedSlot.values()[0]);
                geoLoc = Location.newInstance((double)p.get('lat'),(double)p.get('lng'));
                return geoLoc;
            } 
            catch(Exception ex) {
                system.debug('#exception=' + ex);
                system.debug('#trace=' + ex.getStackTraceString());
            }
        }
        return null;
    }

    /*
    * Calculate travel time between 2 places
    */
    /*
    public static integer calculateTravelTime(String origin, String destination){
        DistanceResponse response = calculateTravelDistance(new list<String>{origin}, new list<String>{destination});

        Element e = null;
        if (response.status == 'OK') {
            e = response.rows[0].elements[0];         
        }
        return e.duration.val;
    }
    */
    /*
    * calculate distance & travel time from a list of origins to destinations
    */
    /*
    public static DistanceResponse calculateTravelDistance(list<string> originAddresses,  list<string> destinationAddresses){

        String origin =  EncodingUtil.urlEncode(String.join(originAddresses, '|'), 'UTF-8') ;
        String destination =  EncodingUtil.urlEncode(String.join(destinationAddresses, '|'), 'UTF-8') ;
        
        Http http           = new Http();
        HttpRequest req     = new HttpRequest(); 
        HttpResponse res    = new HttpResponse();
        
        string EndPoint = GOO_END_POINT + '/distancematrix/json?mode=driving&origins=' + origin + '&destinations=' + destination  + '&language=en-US&key=' + GOOGLE_API_KEY;
        //String EndPoint = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=3920+Shenfield+Drive%2C+Union+City%2C+Georgia%2C+30291+United+States%7C105+New+Oakridge+Trail%2C+Fayetteville%2C+Georgia%2C+30214+United+States%7C3865+Maple+Wood+Trl%2C+Buford%2C+Georgia%2C+30518+United+States%7C6955+Kimberly+Mills+Rd%2C+College+Park%2C+Georgia%2C+30349+United+States%7C1723+Beck+Wit+Place%2C+Charlotte%2C+North+Carolina%2C+28205+United+States%7C210+Cleveland+Ave+-+Appartment+2B%2C+King%27s+Mountain%2C+North+Carolina%2C+28086+United+States%7C45R+-+400+w+43+st%2C+New+York%2C+New+York%2C+10036+United+States%7C1761+50th+Street%2C+Brooklyn%2C+New+York%2C+11204+United+States&destinations=4233+Donna+Way%0D%0ALithonia%2C+Georgia+30038%0D%0AUnited+States%7C1716+Sotogrande+Blvd%2C+Apt+235%0D%0AHurst%2C+Texas+76053%0D%0AUnited+States%7C7401-8223+Clifton+Lane%0D%0ASmithfield%2C+VA+23430%2C+USA%7C7915+Pickering+Avenue%0D%0AWhittier%2C+CA+90602%2C+USA%7C4503+Panther+Place%0D%0ACharlotte%2C+North+Carolina+28269%0D%0AUnited+States%7C83+Willow+Park%2C+Monticello%2C+Kentucky+42633%7C2+Haven+Plaza%0D%0ANew+York+City%2C+New+York+10009%0D%0AUnited+States%7C2245+larkin+st%2C+san+francisco%2C+ca+94109%7C89+Cormorant+Court%0D%0AMartinez%2C+California+94553%0D%0AUnited+States%7C1879+Platte+River+Lane+%232%0D%0AChula+Vista%2C+California+91913%0D%0AUnited+States%7C7700+Ingram+Rd%2C+Apartment+1501%0D%0ASan+Antonio%2C+Texas+78251%0D%0AUnited+States%7C156+Front+Street+West%2C+Toronto%2C+Ontario%2C+Canada%7C144+Fairfield+Circle+West%2C+Royersford%2C+Pennsylvania+19468%2C+United+States%7CNewton%2C+Massachusetts%2C+United+States%7CAppt+1114+5851+Holmberg+Road%0D%0AParkland%2C+Florida+33067%0D%0AUnited+States%7C1451+Tollis+Parkway+%23149%0D%0ABroadview%2C+Ohio+44147%0D%0AUnited+States&language=en-US&key=';
        //EndPoint += conf.Google_Maps_API_Key__c;

        req.setEndpoint( EndPoint );
        req.setMethod('GET');
        req.setTimeout(10000);
        
        integer responseCode = 0;
        string jsonResponse = ''; 
        
        res = http.send(req);
        responseCode = res.getStatusCode(); 
        system.debug('===res: ' + res);
        if(responseCode == 200){
            try{
                jsonResponse = res.getBody();
                return (DistanceResponse)JSON.deserialize(jsonResponse, DistanceResponse.class);
            } catch(Exception ex){
                //Handling exception  
            }   
        }
        
        return null;
    }

    public class Distance{
        public string text { get; set; }
        public double value { get; set; }
        public double val { 
            get{
                double d = double.valueOf(value) * M_TO_MILES;
                if(d < 1)  d = 1;
                return d;
            }
            set; 
        }

    }
    //Data models for Distance matrix 
    public class Duration{
        public string text { get; set; }
        public integer value { get; set; }
        public integer val{
            get{
                integer v = math.round(Double.valueOf(value)/60);
                return v;
            }
            set;
        }
    }

    public class Element {
        public Distance distance { get; set; }
        public Duration duration { get; set; }
        public string status { get; set; }

    }

    public class Row{
        public List<Element> elements { get; set; }
    }

    public class DistanceResponse{
        public List<string> destination_addresses { get; set; }
        public List<string> origin_addresses { get; set; }
        public List<Row> rows { get; set; }
        public string status { get; set; }
        public string error_message {get;set;}
    }
    */
}